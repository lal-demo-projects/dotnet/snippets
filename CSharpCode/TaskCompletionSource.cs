using System;
using System.Threading.Tasks;
using System.Timers;

namespace ConsoleTCS
{
    public class TCSTimer
    {
        public Timer timer = null;
        public TCSTimer()
        {
            timer = new Timer
            {
                AutoReset = false
            };
        }
        public Task<double> RunTimer(TimeSpan timeSpan)
        {
            var tcs = new TaskCompletionSource<double>();

            timer.Interval = timeSpan.TotalMilliseconds;
            void elapsedHandler(object sender, ElapsedEventArgs e) {
                timer.Stop();
                tcs.SetResult(timeSpan.TotalSeconds);
            }
            timer.Elapsed += elapsedHandler;
            timer.Start();

            var result = tcs.Task.ContinueWith((x) => {
                timer.Elapsed -= elapsedHandler;
                return x.Result;
            });

            return result;
        }
    }
}
