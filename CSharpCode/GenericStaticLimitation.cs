namespace GenericCSVParser
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var players = ProcessGenCSV<Player>("Player.csv");

            foreach (var player in players)
            {
                Console.WriteLine(player.PlayerName + ' ' + player.Country);
            }

            Console.ReadLine();
        }

        // TODO It will not work due to language structure and limitations
        private static List<T> ProcessGenCSV<T>(string path) where T : class, ICSVDataObject<T>
        {
            return File.ReadAllLines(path)
                .Skip(1)
                .Where(row => row.Length > 0)
                .Select(x => ICSVDataObject<T>.ParseRow(x)).ToList();
        }
    }

    public interface ICSVDataObject<T>
    {
        static T ParseRow(string row) => throw new NotImplementedException();
    }

    public class Player : ICSVDataObject<Player>
    {
        public int Id { get; set; }
        public string PlayerName { get; set; }
        public string PayerDOB { get; set; }
        public string BattingHand { get; set; }
        public string BowlingSkills { get; set; }
        public string Country { get; set; }
        public string IsUmpire { get; set; }

        public static Player ParseRow(string row)
        {
            var columns = row.Split(',');
            return new Player()
            {
                Id = int.Parse(columns[0]),
                PlayerName = columns[1],
                PayerDOB = columns[2],
                BattingHand = columns[3],
                BowlingSkills = columns[4],
                Country = columns[5],
                IsUmpire = columns[6]
            };
        }
    }
}
